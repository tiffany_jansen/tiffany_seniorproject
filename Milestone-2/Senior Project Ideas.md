Senior Project Ideas
====================

## Idea 1: Workout site

**Basic Information:** Create a hub for workout videos (possibly pulling from YouTube) for example, if a user wanted to find videos to use for working out specific muscle groups, "biceps", or something as generic as "leg exercises" they could.  We could add filtering options by: popularity, time constraints, gym, YouTuber Name. We could have a page for workout scheduling and planning and push daily suggested videos, again based on time constraints, etc… just something that could be useful for people who want to workout more effectively but don't know what to search for on YouTube.

**Questions:**

1. _What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)._  
    * Most similar ideas have to be paid for, many sites offer specific workout plan or programs that you sign up for that offer you videos if you join or pay a fee.  In addition, it's not customizable as this concept.  You are locked into a specific length, a specific type of focus based on what “they” think is best for you.  Our solution allows you to alter your search at any time and track which videos you do and do not like.  This allow for more customized workouts based on preference vs. someones predetermined set of workout videos.  We even found websites that had many walk through workouts for each area of your body, or strength vs. cardio, etc...but no videos, this again was usually linked to a specific program or company.  
2. _Why is this idea worth doing? Why is it useful and not boring?_  
    * Because it looks to solve a problem, people have fast paced lives and many of them want to find a way to build in a workout into their day.  If we can get an idea of what their timeline is, we can make recommendations based on specific filters that the user sets. We can go one step further and make recommendations, then eventually pre schedule for the user.  
3. _What are a few major features?_  
    * Keyword filters that allows a user to specify a type of workout, a body part or muscle group.  
    * Time range filters, ie. I only have 10 mins, only providing the user with videos 10 mins or less.  
    * Tracking, once the user has been to the site more than once, a suggestion list could be provided.  
    * Further expanding on the suggestion list is the idea of preplanning a workout list for the user.  If the user seems to work out Mon, Wed, Fri, and usually does videos around 30 mins, these could be pre set for the user as soon as they log in an go to a schedule page.   
4. _What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use._ 
    * Youtube API.  Possibly some sort of scheduling platform that includes an interactive calendar  
5. _What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)_  
    * There may be some algorithmic work involved with filtering, including time, muscle groups, etc…  
    * Most of the algorithmic work would be in the pre-planning or scheduling component for the user before hand. Data would need to be collected including their login information and search history.  We would need to use their history to track previous selections and determine the best workout suggestions based on their choices, trends, favorite workout person, areas of their body, etc...in order to provide a valid and useful set of predetermined suggestions.  Then scheduling would need to be provided, the user could input their workout timeline preferences, then we could insert videos proactively based on their history or predetermined  choices aka arms, Mon, abs Tues, etc...  
6. _Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully._  
    * 6-8 depending how hard the preplanning component would be and how accurate we were able to make it.  Also integrating another scheduling platform may be challenging.  

## Idea 2: Travel site

**Basic Information:** A website that allows you to see where you can afford to travel given a specific budget and number of people attending. It would do a currency exchange, check airline prices, and give you weather info for the weather in the area. It would even give you some cool things to do in each spot and figure out how much a hotel would cost. 

**Questions:**

1. _What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)._   
    * Takes the guesswork out of travelling. You would already know that you can afford to go there, so why not go?
    * There are sites like it out there, but they only focus on one thing. Ex: Kayak Map only focuses on the airline prices, not the hotel, or the currency exchange. 
2. _Why is this idea worth doing? Why is it useful and not boring?_
    * It would assist in helping users to budget travel possibilities more easily.  A one stop shop for expenses based on location, number of people, and travel duration.
3. _What are a few major features?_
    * Currency Exchange for Country to be visited (if the currency is different)
    * Weather of country/place to be visited (average weather from the area around the time of travel)
    * Checking average airline prices for going to country/place
    * Checking average hotel costs for the number of people going in the specific country/place.
    * Include pictures of the country/place to be visited.
    * Ideas of things that people can do while in that country/place.
    * Allowing people to choose a specific area that they wanna visit.
4. _What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use._
    * Currency Exchange API ~ Like: Currencylayer, or Exchangeratesapi.io
    * Travel API for Airline Info ~ Maybe: Schiphol Airport
    * Weather API ~ Like: Dark Sky, or OpenWeatherMap
    * Maybe do something with Trivago/Priceline to get pricing for Hotels and Flights.
5. _What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)_
    * We would have to make an algorithm that calculates how much it would cost for the person to go wherever and makes sure that the budget given is going to be enough. This could be entered in as a range by the user and we could produce an amount expected that would include information whether they were under or over budget. 
6. _Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully._
    * Probably about a 5-7 since we would be using so many APIs and the algorithm would have to check a lot of information.

## Idea 3: Anime Site

**Basic Information:** Same idea as above (#1) except tailored to anime. Like you search for an anime to get animes from similar genre/production studio/voice actors and what season they came out, how many eps they are, etc. Could also add a scheduler for the current season of anime (air dates, what streaming service it’s available on, etc) where  people can add which anime they are watching to their schedule etc. We could also add stuff that allows the user to select things they are interested in and gives animes that the user would likely enjoy to watch. This could also include a YouTube API integration to link the OP/ED animation/songs and link to others by the same artist. Include a random anime selector based upon which tags user frequently adds to their list.

**Questions:**

1. _What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)._    
    * There are a few anime list websites (MyAnimeList, MyAniChart, MyAniList) but they do not have personalized calendar tracking for the current anime season and they do not Crunchyroll or Funimation integration (such as listing where the anime can be watched legally). 
2. _Why is this idea worth doing? Why is it useful and not boring?_    
    * There are a few anime tracking websites but they aren’t as personalized as they could be in regards to the current season of anime (other websites list all currently airing anime which clutters up the homepage, I would like to make a customized calendar tracking system).   
3. _What are a few major features?_    
    * User Accounts with a Dashboard that contains "Followed" anime, next to be "aired" with a countdown to the date, and group updates, a Queue, a History of finished and reviews of the animes watched, and a Calendar with listing and phone alerts.
    * Search Bar with searching by genre, specific animes, and by season.
    * Links to the pages with the anime where you can watch legally like Crunchyroll/Funimation where you can add anime to watch queue with a link.
    * Seasonal Calendar for scheduling what you are watching and when it comes out that would include database integration with my anime list and an anime detail page where the user can add to queue, look at the description, characters (and voice actor, crunchy details (Scores, season, episode length, etc.)
    * Create Groups "Weeb Watch" where multiple users can add stuff to the list, make notes on anime like mark as seen, group admin that can call a vote to find what to watch next, Mark what episode each group member is in, and create forum discussions(?)  
4.  _What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use._    
    * https://myanimelist.net/forum/?topicid=1740204 (My Anime List API)
    * https://www.dnnsoftware.com/ (Open source CMS)
    * Google Calendar for the seasonal calendar     
5.  _What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)_    
    * An algorithm to take what you’ve watched in the past and come up with similar ones that the site thinks you would enjoy. 
    * Algorithm to remove the “extra” information from the APIs that we don’t need.   
6.  _Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully._    
    * 5-8 depending on how far we wanna go with the APIs for the animes and how hard creating the calendar and user accounts will be.  