namespace Test.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EFUsersContext : DbContext
    {
        public EFUsersContext()
            : base("name=EFUsersContext")
        {
        }

        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Discussion> Discussions { get; set; }
        public virtual DbSet<FeedbackComment> FeedbackComments { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasMany(e => e.FeedbackComments)
                .WithRequired(e => e.Comment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Discussion>()
                .HasMany(e => e.Articles)
                .WithRequired(e => e.Discussion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Discussion>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Discussion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Discussion>()
                .HasMany(e => e.FeedbackComments)
                .WithRequired(e => e.Discussion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Discussions)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatorID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.FeedbackComments)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
