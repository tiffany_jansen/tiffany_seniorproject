﻿CREATE TABLE [dbo].[Users]
(
	[UserID] INT IDENTITY (1,1) NOT NULL,
	[Email] NVARCHAR(100) NOT NULL,
	[Password] NVARCHAR(30) NOT NULL,
	[FirstName] NVARCHAR(30) NOT NULL,
	[LastName] NVARCHAR(30) NOT NULL,
	[Admin] BIT NOT NULL,
	[Moderator] BIT NOT NULL,

	CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
	);

	CREATE TABLE [dbo].[Discussions]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Title] NVARCHAR(MAX) NOT NULL,
	[CreatorID] INT NOT NULL,
	[Timestamp] DATETIME NOT NULL,
	[Text] NVARCHAR(MAX) NOT NULL,

	CONSTRAINT [PK_dbo.Discussions] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_dbo.Discussions] FOREIGN KEY (CreatorID) REFERENCES [dbo].[Users] (UserID)
	);

	CREATE TABLE [dbo].[Articles]
(
	[ID] INT IDENTITY (1001,1) NOT NULL,
	[Title] NVARCHAR(MAX) NOT NULL,
	[URL] NVARCHAR(100) NOT NULL,
	[Author] NVARCHAR(50) NOT NULL,
	[DiscussionID] INT NOT NULL,

	CONSTRAINT [PK_dbo.Articles] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_dbo.Articles] FOREIGN KEY (DiscussionID) REFERENCES [dbo].[Discussions] (ID)
	);

	CREATE TABLE [dbo].[Comments]
	(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Timestamp] DATETIME NOT NULL,
	[UserID] INT NOT NULL,
	[DiscussionID] INT NOT NULL,
	[UpVote] INT NOT NULL,
	[DownVote] INT NOT NULL,
	[Fact] INT NOT NULL,
	[CounterFact] INT NOT NULL,
	[General] INT NOT NULL,
	[OpinionBased] INT NOT NULL,
	[Complaint] INT NOT NULL,
	[Helpful] INT NOT NULL,
	[Unhelpful] INT NOT NULL,
	[Trolling] INT NOT NULL,
	[Rude] INT NOT NULL,
	[Inappropriate] INT NOT NULL,
	
	CONSTRAINT [PK_dbo.Comments] PRIMARY KEY CLUSTERED (ID ASC),
    CONSTRAINT [FK_dbo.Comments] FOREIGN KEY (UserID) REFERENCES [dbo].[Users] (UserID),
	CONSTRAINT [FK2_dbo.Comments] FOREIGN KEY (DiscussionID) REFERENCES [dbo].[Discussions] (ID)
	);

	CREATE TABLE [dbo].[FeedbackComments]
	(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[UserID] INT NOT NULL,
	[Text] NVARCHAR (MAX) NOT NULL,
	[Type] INT NOT NULL,
	[DiscussionID] INT NOT NULL,
	[CommentID] INT NOT NULL,
	[Processed] BIT NOT NULL,
	
	CONSTRAINT [PK_dbo.FeedbackComments] PRIMARY KEY CLUSTERED (ID ASC),
    CONSTRAINT [FK_dbo.FeedbackComments] FOREIGN KEY (UserID) REFERENCES [dbo].[Users] (UserID),
	CONSTRAINT [FK2_dbo.FeedbackComments] FOREIGN KEY (DiscussionID) REFERENCES [dbo].[Discussions] (ID),
	CONSTRAINT [FK3_dbo.FeedbackComments] FOREIGN KEY (CommentID) REFERENCES [dbo].[Comments] (ID)
	);

	INSERT INTO [dbo].[Users] (Email, Password, FirstName, LastName, Admin, Moderator) VALUES
		('JaneStone@stuff.com','12345','Jane', 'Stone', 0, 0),
		('JohnSmill@stuff.com','12345','John', 'Smith', 1, 1),
		('BobDaniels@stuff.com','12345','Bob', 'Daniels', 0, 1)

	INSERT INTO [dbo].[Discussions](Title, CreatorID, Timestamp, Text) VALUES
		('Dude, wheres my car?', 1, '12/04/2017 09:04:22', 'This article stinks!'),
		('Trump tweets again!', 1, '10/04/2018 12:04:22', 'Craziness'),
		('We never had a chance', 2, '1/04/2019 11:04:22', 'Or did we?')

	INSERT INTO [dbo].[Articles](Title, URL, Author, DiscussionID) VALUES
		('News Article 1', 'https://drive.google.com/drive/folders/1X1kbH56fSj229LiVbvwptq0Eg3bNr-Hn', 'Jane Stone', 1),
		('News Article 2', 'https://drive.google.com/drive/folders/1X1kbH56fSj229LiVbvwptq0Eg3bNr-Hy', 'Tom McMasters', 2),
		('News Article 3', 'https://drive.google.com/drive/folders/1X1kbH56fSj229LiVbvwptq0Eg3bNr-Hz', 'Otto Vanderwall', 3)

	INSERT INTO [dbo].[Comments](Timestamp, UserID, DiscussionID, UpVote, DownVote, Fact, CounterFact, General, OpinionBased, Complaint, Helpful, Unhelpful, Trolling, Rude, Inappropriate) VALUES
		('12/04/2017 09:04:22', 1, 1, 20, 0, 20, 0, 100, 20, 5, 2, 100, 80, 60, 40),
		('10/04/2018 12:04:22', 2, 3, 0, 20, 10, 3, 50, 15, 10, 5, 65, 33, 22, 11),
		('1/04/2019 11:04:22', 3, 2, 11, 2, 56, 4, 187, 99, 4, 5, 89, 65, 45, 3)
	
	INSERT INTO [dbo].[FeedbackComments](UserID, Text, Type, DiscussionID, CommentID, Processed) VALUES
	(1, 'This guy is a jerk', 1, 1, 2, 0),
	(2, 'Mean, mean, mean', 2, 2, 1, 0),
	(3, 'This guy is a jerk', 3, 3, 2, 1)

		GO