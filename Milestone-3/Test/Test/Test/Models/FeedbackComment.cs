namespace Test.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FeedbackComment
    {
        public int ID { get; set; }

        public int UserID { get; set; }

        [Required]
        public string Text { get; set; }

        public int Type { get; set; }

        public int DiscussionID { get; set; }

        public int CommentID { get; set; }

        public bool Processed { get; set; }

        public virtual Comment Comment { get; set; }

        public virtual Discussion Discussion { get; set; }

        public virtual User User { get; set; }
    }
}
