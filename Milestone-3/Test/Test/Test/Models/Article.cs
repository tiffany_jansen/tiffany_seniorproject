namespace Test.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Article
    {
        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        public string URL { get; set; }

        [Required]
        [StringLength(50)]
        public string Author { get; set; }

        public int DiscussionID { get; set; }

        public virtual Discussion Discussion { get; set; }
    }
}
