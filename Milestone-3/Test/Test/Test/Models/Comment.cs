namespace Test.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Comment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Comment()
        {
            FeedbackComments = new HashSet<FeedbackComment>();
        }

        public int ID { get; set; }

        public DateTime Timestamp { get; set; }

        public int UserID { get; set; }

        public int DiscussionID { get; set; }

        public int UpVote { get; set; }

        public int DownVote { get; set; }

        public int Fact { get; set; }

        public int CounterFact { get; set; }

        public int General { get; set; }

        public int OpinionBased { get; set; }

        public int Complaint { get; set; }

        public int Helpful { get; set; }

        public int Unhelpful { get; set; }

        public int Trolling { get; set; }

        public int Rude { get; set; }

        public int Inappropriate { get; set; }

        public virtual User User { get; set; }

        public virtual Discussion Discussion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedbackComment> FeedbackComments { get; set; }
    }
}
