namespace classproj.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Discussion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Discussion()
        {
            Articles = new HashSet<Article>();
            Comments = new HashSet<Comment>();
            FeedbackComments = new HashSet<FeedbackComment>();
        }

        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        public int CreatorID { get; set; }
        private DateTime date = DateTime.Now;
        [Display(Name = "Submission Time")]
        public DateTime Timestamp
        {
            get { return date; }
            set { date = value; }
        }

        [Required]
        public string Text { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Article> Articles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeedbackComment> FeedbackComments { get; set; }
    }
}
